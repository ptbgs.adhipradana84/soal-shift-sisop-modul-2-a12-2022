#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <time.h>
#include <string.h>
#include <dirent.h>
#include <json-c/json.h>

void jsonPar(char format[], int gacha, int primogems, int counter){
	FILE *fp;
	char buffer[100000];
	struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;
	
	fp = fopen("./characters/aether.json", "r");
	fread(buffer, 100000, 1, fp);
	fclose(fp);
	
	parsed_json = json_tokener_parse(buffer);
	
	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);
	
	if(counter % 2 == 0){
		fp = fopen(format, "a+");
		fprintf(fp, "%d_weapons_%d_%s_%s\n", counter, primogems, json_object_get_string(name), json_object_get_string(rarity));
	
		fclose(fp);
	}else{
		fp = fopen(format, "a+");
		fprintf(fp, "%d_character_%d_%s_%s\n", counter, primogems, json_object_get_string(name), json_object_get_string(rarity));
	
		fclose(fp);
	}
}

int main(){
	
	int primogems = 79000;
	int cost = 160;
	int gacha = 0;
	int gachaCounter = 0;
	char tempName[500];
	char buffer1[500];
	char buffer2[500];
	int i;
	
	time_t jikan;
	struct tm * timeStamp;
			
	time(&jikan);
	timeStamp = localtime(&jikan);
	
	int status;
	
	char *name[] = {
		"characters.zip", 
		"weapons.zip"
	};
	
	char *link[] = {
		"https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
		"https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"
	};	
	
	for(int i = 0; i < 2; i++){
		if(fork() == 0){
			execlp("wget", "wget", "-q", "--no-check-certificate", link[i], "-O", name[i], NULL);
		} 	
		
		while(wait(&status) > 0);
		
		if(fork() == 0){
			execlp("unzip", "unzip", name[i], NULL);
		}
		
		while(wait(&status) > 0);
		
		if(fork() == 0){
			execlp("rm", "rm", name[i], NULL);
		}
		
		while(wait(&status) > 0);
	}
	
	if(fork() == 0){
		execlp("mkdir", "mkdir", "gacha_gacha", NULL);
	} 	
		
	while(wait(&status) > 0);
	
	do{
		gacha += 1;
		
		if(gacha % 10 == 0){
			sprintf(buffer1, "gacha_gacha/%02d:%02d:%02d_gacha_%d.txt", timeStamp->tm_hour, timeStamp->tm_min, timeStamp->tm_sec, gacha);
			
			if(fork() == 0){
				execlp("touch", "touch", buffer1, NULL);
			}
			
			while(wait(&status) > 0);
			
			for(i = 0; i < 10; i++){
				primogems -= cost;
				gachaCounter += 1;
				if(gachaCounter % 2 == 0){
					jsonPar(buffer1, gacha, primogems, gachaCounter);
				}else{
					jsonPar(buffer1, gacha, primogems, gachaCounter);
				}
			}
		}
		
		if(gacha % 90 == 0){
			sprintf(buffer2, "gacha_gacha/total_gacha_%d", gacha);
			
			if(fork() == 0){
				execlp("mkdir", "mkdir", buffer2, NULL);
			}
			
			while(wait(&status) > 0);
			
		}	
		
	}while(primogems >= 0);
	
	if(timeStamp->tm_mday == 30 &&
	   timeStamp->tm_mon == 3 &&
	   timeStamp->tm_hour == 7 &&
	   timeStamp->tm_min == 44){
		
		if(fork()==0){
			execlp("zip", "zip", "-rmvqP", "satuduatiga", "-r", "not_safe_for_wibu", "gacha_gacha",NULL);
		}
		while(wait(&status) > 0)
		
		if(fork() == 0){
			execlp("rm", "rm", "-rf", "characters", NULL);
		}
		
		while(wait(&status) > 0);
		
		if(fork() == 0){
			execlp("rm", "rm", "-rf", "weapons", NULL);
		}
		
		while(wait(&status) > 0);
		
		
	}
	return 0;
}
