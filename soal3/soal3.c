#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>
#include <sys/types.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>

char *getUID(char path[])
{
    struct stat info;
    int r;

    r = stat(path, &info);
    if (r == -1)
    {
        fprintf(stderr, "File error\n");
        exit(1);
    }

    struct passwd *pw = getpwuid(info.st_uid);

    return pw->pw_name;
}

char *getFilePermission(char path[], char permission[])
{
    struct stat fs;
    int r;

    r = stat(path, &fs);
    if (r == -1)
    {
        fprintf(stderr, "File error\n");
        exit(1);
    }

    if (fs.st_mode & S_IRUSR)
        strcat(permission, "r");
    if (fs.st_mode & S_IWUSR)
        strcat(permission, "w");
    if (fs.st_mode & S_IXUSR)
        strcat(permission, "x");

    return permission;
}

void formatFileName(char fullPath[], char name[], FILE *fptr)
{
    char fullFileName[50] = "";
    char permission[4] = "";
    strcat(fullFileName, getUID(fullPath));
    strcat(fullFileName, "_");
    strcat(fullFileName, getFilePermission(fullPath, permission));
    strcat(fullFileName, "_");
    strcat(fullFileName, name);
    fprintf(fptr, "%s", fullFileName);
    fprintf(fptr, "%s", "\n");
}

void makeDirectory(char path[])
{
    int status;
    if (fork() == 0)
        execlp("mkdir", "mkdir", "-p", path, NULL);
    while ((wait(&status)) > 0)
        ;
}

void unzip(char destination[], char target[])
{
    int status;
    if (fork() == 0)
        execlp("unzip", "unzip", "-q", "-d", destination, target, NULL);

    while ((wait(&status)) > 0)
        ;
}

void moveFile(char source[], char destination[])
{
    int status;
    if (fork() == 0)
        execlp("mv", "mv", source, destination, NULL);
    while ((wait(&status)) > 0)
        ;
}

void removeFile(char source[])
{
    int status;
    if (fork() == 0)
        execlp("rm", "rm", "--force", source, NULL);
    while ((wait(&status)) > 0)
        ;
}

void makeFile(char fileName[])
{
    int status;
    if (fork() == 0)
        execlp("touch", "touch", fileName, NULL);
    while ((wait(&status)) > 0)
        ;
}

void traverseFilesAndDoSomething(char path[], char type[])
{
    DIR *dp;
    struct dirent *ep;

    dp = opendir(path);

    FILE *fptr; // only used in writeAirAnimals
    if (strcmp(type, "writeAirAnimals") == 0)
    {
        char pathListText[100] = "";
        strcat(pathListText, path);
        strcat(pathListText, "list.txt");
        fptr = fopen(pathListText, "w");
        if (fptr == NULL)
        {
            printf("Error!");
            exit(1);
        }
    }

    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            if (strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, ".") == 0)
                continue;

            if (strcmp(type, "categorizingAnimals") == 0)
            {
                char source[100] = "";
                strcat(source, path);
                strcat(source, ep->d_name);
                char dest[100] = "/home/rocuz99/modul2/";
                if (strstr(ep->d_name, "darat"))
                {
                    strcat(dest, "darat/");
                    strcat(dest, ep->d_name);
                    moveFile(source, dest);
                }
                else if (strstr(ep->d_name, "air"))
                {
                    strcat(dest, "air/");
                    strcat(dest, ep->d_name);
                    moveFile(source, dest);
                }
                else
                {
                    removeFile(source);
                }
            }
            else if (strcmp(type, "removeBirdFile") == 0)
            {
                char filePath[100] = "";
                strcat(filePath, path);
                if (strstr(ep->d_name, "bird"))
                {
                    strcat(filePath, ep->d_name);
                    removeFile(filePath);
                }
            }
            else if (strcmp(type, "writeAirAnimals") == 0)
            {
                if (strcmp(ep->d_name, "..") != 0 && strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "list.txt") != 0)
                {
                    char fullPath[100] = "";
                    strcat(fullPath, path);
                    strcat(fullPath, "/");
                    strcat(fullPath, ep->d_name);

                    formatFileName(fullPath, ep->d_name, fptr);
                }
            }
            else
            {
                perror("Error: Type not found\n");
            }
        }

        (void)closedir(dp);
    }
    else
        perror("Couldn't open the directory");

    if (strcmp(type, "writeAirAnimals") == 0)
    {
        fclose(fptr);
    }
}

int main()
{
    makeDirectory("/home/rocuz99/modul2/darat");
    sleep(3);
    makeDirectory("/home/rocuz99/modul2/air");

    unzip("/home/rocuz99/modul2", "./animal.zip");

    traverseFilesAndDoSomething("/home/rocuz99/modul2/animal/", "categorizingAnimals");
    traverseFilesAndDoSomething("/home/rocuz99/modul2/darat/", "removeBirdFile");

    makeFile("/home/rocuz99/modul2/air/list.txt");

    traverseFilesAndDoSomething("/home/rocuz99/modul2/air/", "writeAirAnimals");

    return 0;
}