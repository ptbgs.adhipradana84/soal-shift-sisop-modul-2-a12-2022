# soal-shift-sisop-modul-1-A12-2022

### Member
- Christhoper Marcelino Mamahit (5025201249)
- I Putu Bagus Adhi Pradana (5025201010)
- Yusron Nugroho Aji (5025201138)

## Soal 1 (Belum Selesai)

Pada soal no 1 kami diminta untuk membuat sebuah program simulasi gacha. Pada program ini tidak hanya digunakan untuk melahkukan gacha melainkan ada beberapa task yang juga perlu dilakukan. Namun sebelum itu dilahkukan bebrapa pendeklarasian variable terlebih dahulu.
```c
int primogems = 79000;
int cost = 160;
int gacha = 0;
int gachaCounter = 0;
char tempName[500];
char buffer1[500];
char buffer2[500];
	
time_t jikan;
struct tm * timeStamp;
			
time(&jikan);
timeStamp = localtime(&jikan);
	
int status;
char *name[] = {
	"characters.zip", 
	"weapons.zip"
};
	
char *link[] = {
	"https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
	"https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"
};	

```
Setelah itu barulah dilakukan task tasknya.

* Mendownload, mengestrak, dan menghapus file zip yang sudah diekstrak untuk karakter dan senjata dari web berikut.
   
  * Database item [characters](https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view)
  * Database item [weapons](
https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view)
```c
for(int i = 0; i < 2; i++){
	if(fork() == 0){
		execlp("wget", "wget", "--no-check-certificate", link[i], "-O", name[i], "-q", NULL);
	} 	
		
	while(wait(&status) > 0);
	
	if(fork() == 0){
		execlp("unzip", "unzip", name[i], NULL);
	}
		
	while(wait(&status) > 0);
		
	if(fork() == 0){
		execlp("rm", "rm", name[i], NULL);
	}
		
	while(wait(&status) > 0);
}

while(wait(&status) > 0);
```
* Membuat directory gacha_gacha sebagai working directory dimana semua hasil gacha nanti akan disimpan disini.

```c
if(fork() == 0){
	execlp("mkdir", "mkdir", "gacha_gacha", NULL);
} 	
		
while(wait(&status) > 0);

```
* Setelah itu dilahkukan looping untuk melahkukan gacha dan dilahkukan beberapa penambahan dan pengurangan dari variable yang sudah dideklarasi. Gacha akan berlangsung selama primogems bernilai lebih dari nol.
```c
do{
	primogems -= (cost * 10);
	gacha += 10;
	gachaCounter += 1;

    ....

}while(primogems >= 0);
```
* Gacha akan dilahkukan bergiliran, dimana nanti jika ganjil akan menggacha karakter dan genap menggacha senjata.(Belum terpenuhi).

*  Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Juga file tersebut akan diberi nama sesuai dengan format {Hh:Mm:Ss}_gacha_{jumlah-gacha}.

```c
        if(gacha % 10 == 0){
			sprintf(buffer1, "gacha_gacha/%02d:%02d:%02d_gacha_%d.txt", timeStamp->tm_hour, timeStamp->tm_min, timeStamp->tm_sec, gacha);
			
			if(fork() == 0){
				execlp("touch", "touch", buffer1, NULL);
			}
			
			while(wait(&status) > 0);
			
			for(i = 0; i < 10; i++){
				primogems -= cost;
				gachaCounter += 1;
				if(gachaCounter % 2 == 0){
					jsonPar(buffer1, gacha, primogems, gachaCounter);
				}else{
					jsonPar(buffer1, gacha, primogems, gachaCounter);
				}
			}
		}

``` 
* Setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut. Juga akan diberikan format penamaan total_gacha_{jumlah-gacha}.

```c
if(gacha % 90 == 0){
	sprintf(buffer2, "gacha_gacha/total_gacha_%d", gacha);
			
	if(fork() == 0){
		execlp("mkdir", "mkdir", buffer2, NULL);
	}
			
	while(wait(&status) > 0);
			
}	

```
* Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}.

```c
void jsonPar(char format[], int gacha, int primogems, int counter){
	FILE *fp;
	char buffer[100000];
	struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;
	
	fp = fopen("./characters/aether.json", "r");
	fread(buffer, 100000, 1, fp);
	fclose(fp);
	
	parsed_json = json_tokener_parse(buffer);
	
	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);
	
	if(counter % 2 == 0){
		fp = fopen(format, "a+");
		fprintf(fp, "%d_weapons_%d_%s_%s\n", counter, primogems, json_object_get_string(name), json_object_get_string(rarity));
	
		fclose(fp);
	}else{
		fp = fopen(format, "a+");
		fprintf(fp, "%d_character_%d_%s_%s\n", counter, primogems, json_object_get_string(name), json_object_get_string(rarity));
	
		fclose(fp);
	}
}
```

* Proses gacha akan dimulai 30 maret 4:44 dan berakhir 3 jam setelahnya. Dimana nantinya folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan didelete sehingga hanya menyisakan file (.zip).

```c
    if(timeStamp->tm_mday == 30 &&
	   timeStamp->tm_mon == 3 &&
	   timeStamp->tm_hour == 7 &&
	   timeStamp->tm_min == 44){
		
		if(fork()==0){
			execlp("zip", "zip", "-rmvqP", "satuduatiga", "-r", "not_safe_for_wibu", "gacha_gacha",NULL);
		}
		while(wait(&status) > 0)
		
		if(fork() == 0){
			execlp("rm", "rm", "-rf", "characters", NULL);
		}
		
		while(wait(&status) > 0);
		
		if(fork() == 0){
			execlp("rm", "rm", "-rf", "weapons", NULL);
		}
		
		while(wait(&status) > 0);
		
		
	}

```

### Permasalahan dalam pengerjaan soal 1



## Soal 2 (Belum Selesai)
Pada soal ini, kami diminta membuat program untuk mengklasifikasi file dari drakor.zip ke kategorinya masing-masing dan mencetak daftarnya ke dalam sebuah file txt.

```c
int main()
{
    makeDirectory("/home/rocuz99/shift2/drakor");
    unzip("/home/rocuz99/shift2/drakor", "./drakor.zip");

    traverseFilesAndDoSomething("/home/rocuz99/shift2/drakor/");

    return 0;
}

```
Pertama, kami membuat dahulu folder drakor yang akan dijadikan tempat untuk folder genre nya . Lalu, melakukan unzip file drakor.zip. File akan ada di dalam folder drakor dan berisikan folder genre nya baik dari romance, action dan yang lain. Setelah itu, dilakukan pengaktegorian untuk file tersebut ke dalam folder genrenya masing-masing dan merenamenya dilanjutkan menuliskan daftar judul drama tersebut ke dalam file txt.

```c
void makeDirectory(char path[])
{
    int status;
    if (fork() == 0)
        execlp("mkdir", "mkdir", "-p", path, NULL);
    while ((wait(&status)) > 0)
        ;
}

```

Kami membuat function makeDrectory untuk dapat menjalankan fork untuk fungsi exec, yaitu dengan fungsi fork dapat digunakan untuk menghasilkan child proses yang dapat mengeksekusi mkdir di dalam exec tersebut untuk dapat menghasilkan folder di path yang diharapkan.

```c
void unzip(char destination[], char target[])
{
int status;
if (fork() == 0)
    execlp("unzip", "unzip", "-q", "-d", destination, target, NULL);
while ((wait(&status)) > 0);
}
```
Function unzip ini digunakan untuk mengunzip suatu file. Child process akan mengeksekusi unzip di dalam exec untuk meng-unzip sesuai path file ny path tujuan sementara parent process menunggunya selesai.

```c
void moveFile(char source[], char destination[])
{
    int status;
    if (fork() == 0)
        execlp("mv", "mv", source, destination, NULL);
    while ((wait(&status)) > 0)
        ;
}

```

Untuk function moveFile digunakan untuk memindahkan file. function ini ini mengeksekusi mv yang ada di dalam exec untuk memindahkan file sesuai path source dan tujuan yang telah ditentukan sebelumnya.

```c
void makeFile(char fileName[])
{
    int status;
    if (fork() == 0)
    {
        execlp("touch", "touch", fileName, NULL);
    }
    while ((wait(&status)) > 0)
        ;
}

```
Function ini berperan untuk membuat membuat suatu file. Di dalam function ini child proses akan mengeksekusi touch dalam exec untuk membuat file sesuai path telah ditentukan sebelumnya sementara parent process menunggunya selesai.

```c
void traverseFilesAndDoSomething(char path[])
{
    DIR *dp;
    struct dirent *ep;

    dp = opendir(path);

    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            if (strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, ".") == 0 || !strstr(ep->d_name, ".png"))
                continue;

            char fullName[100] = "";
            strcpy(fullName, ep->d_name);

            char *end_token1;
            char *token1 = strtok_r(ep->d_name, "_", &end_token1);
            while (token1 != NULL)
            {
                // the-k2;2016;action.png
                char *end_token2;
                char *token2 = strtok_r(token1, ";", &end_token2);

                // title, year, genre
                char fileData[3][50] = {"", "", ""};

                int index = 0;
                while (token2 != NULL)
                {
                    strcpy(fileData[index], token2);
                    index++;
                    token2 = strtok_r(NULL, ";", &end_token2);
                }

                if (strstr(fileData[2], ".png"))
                    assignWithoutExtension(fileData[2]);

                // make directory
                char dirPath[50] = "/home/rocuz99/shift2/drakor/";
                strcat(dirPath, fileData[2]);
                makeDirectory(dirPath);

                // make data.txt
                strcat(dirPath, "/data.txt");
                if (access(dirPath, F_OK) == 0)
                {
                    writeDataToFile(dirPath, fileData[0], fileData[1]);
                }
                else
                {
                    makeFile(dirPath);
                    writeTitleToFile(dirPath, fileData[2]);
                    writeDataToFile(dirPath, fileData[0], fileData[1]);
                }

                // copy file
                char initialFilePath[100] = "/home/rocuz99/shift2/drakor/";
                strcat(initialFilePath, fullName);

                char targetFilePath[100] = "/home/rocuz99/shift2/drakor/";
                strcat(targetFilePath, fileData[0]);
                strcat(targetFilePath, ".png");
                copyFile(initialFilePath, targetFilePath);

                // move file
                strcpy(initialFilePath, targetFilePath); // swap init and target path

                strcpy(targetFilePath, "/home/rocuz99/shift2/drakor/");
                strcat(targetFilePath, fileData[2]);

                moveFile(initialFilePath, targetFilePath);

                token1 = strtok_r(NULL, "_", &end_token1);
            }
        }

        (void)closedir(dp);
    }
    else
        perror("Couldn't open the directory");
}

```
Di fungsi ini, directory listing terjadi serta melalukan beberapa task yang lain. Dengan bantuan library `dirent.h`, file akan diiterasi dan namanya bisa diakses melalui `ep->d_name`.    
Di dalam fungsi ini dengan mendapatkan data dari suatu file dengan memisahkan tanda ; dari nama file tersebut. untuk mendapatkan data mengenai judul, tahun, dan genrenya. Setelah didapatkan datanya maka akan dilanjut dengan membuat directory dan mebuat file txt nya beserta isinya. Setelah itu file akan dicopy ke folder tujuan yang sesuai dengan genrenya.

```c
void writeDataToFile(char path[], char name[], char year[])
{
    FILE *fptr;
    fptr = fopen(path, "a");
    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }

    fprintf(fptr, "%s", "\n");
    fprintf(fptr, "%s", "nama: ");
    fprintf(fptr, "%s", name);
    fprintf(fptr, "%s", "\n");
    fprintf(fptr, "%s", "rilis: tahun ");
    fprintf(fptr, "%s", year);
    fprintf(fptr, "%s", "\n");

    fclose(fptr);
}
```
Di dalam function ini digunakan untuk membuat daftar dari isi txt nya yang mana data tersebut didapatkan dari nama di file nya yang diambil melalui batas delimiternya dan di print sesuai dengan daftar listnya masing-masng.

```c
void writeTitleToFile(char path[], char genre[])
{
    FILE *fptr;
    fptr = fopen(path, "w");
    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }

    char text[100] = "kategori: ";
    strcat(text, genre);

    fprintf(fptr, "%s", text);
    fprintf(fptr, "%s", "\n");

    fclose(fptr);
}

```
Di dalam function ini digunakan untuk membuat nama dari file txt nya, yang didapatkan dari nama atau jenis genrenya suatu file tersebut. 

```c
void assignWithoutExtension(char genre[])
{
    char *token = strtok(genre, ".");
    strcpy(genre, token);
}

```
Function ini digunakan untuk assign suatu file tanpa melibatkan nama extension yang ada di file tersebut.

### Permasalahan dalam pengerjaan soal 2
* Masih belum menemukan cara supaya daftar file tercatat berurut berdasarkan tahun secara ascending

Revisi

```c
// Priority Queue code
typedef struct pqueueNode_t
{
    int year;
    char title[100];
    struct pqueueNode_t *next;
} PQueueNode;

typedef struct pqueue_t
{
    PQueueNode *_top;
    unsigned _size;
} PriorityQueue;

void pqueue_init(PriorityQueue *pqueue)
{
    pqueue->_top = NULL;
    pqueue->_size = 0;
}

bool pqueue_isEmpty(PriorityQueue *pqueue)
{
    return (pqueue->_top == NULL);
}

void pqueue_push(PriorityQueue *pqueue, int value, char title[])
{
    PQueueNode *temp = pqueue->_top;
    PQueueNode *newNode =
        (PQueueNode *)malloc(sizeof(PQueueNode));
    newNode->year = value;
    strcpy(newNode->title, title);
    newNode->next = NULL;

    if (pqueue_isEmpty(pqueue))
    {
        pqueue->_top = newNode;
        return;
    }

    if (value < pqueue->_top->year)
    {
        newNode->next = pqueue->_top;
        pqueue->_top = newNode;
    }
    else
    {
        while (temp->next != NULL &&
               temp->next->year < value)
            temp = temp->next;
        newNode->next = temp->next;
        temp->next = newNode;
    }
}

void pqueue_pop(PriorityQueue *pqueue)
{
    if (!pqueue_isEmpty(pqueue))
    {
        PQueueNode *temp = pqueue->_top;
        pqueue->_top = pqueue->_top->next;
        free(temp);
    }
}

int pqueue_top_year(PriorityQueue *pqueue)
{
    if (!pqueue_isEmpty(pqueue))
        return pqueue->_top->year;
    else
        return 0;
}

char *pqueue_top_title(PriorityQueue *pqueue)
{
    if (!pqueue_isEmpty(pqueue))
        return pqueue->_top->title;
    else
        return "";
}
// End of Priority Queue code
```
Menginisialisasi priority queue sebagai cara untuk dapat sort hasil yang akan ditulis di txt denagn format berdasarkan tahun rilis drama secara ascending. Di function atas merupakan implementasi dari priority queue yang digunakan untuk melakukan proses seperti pop, push, isEmpty dan ang ain-lain.

```c
void copyFile(char source[], char destination[])
{
    int status;
    if (fork() == 0)
        execlp("cp", "cp", source, destination, NULL);
    while ((wait(&status)) > 0)
        ;
}
```
Function ucopyFile ini digunakan untuk menggandakan atau mengcopy suatu file dan disimpan di folder tujuannya. Child process akan mengeksekusi copy di dalam exec untuk meng-copy sesuai path file ny path tujuan sementara parent process menunggunya selesai.

```c
void addType(char type[])
{
    // add type (romance, action, etc) to types array, but same value is not allowed
    for (int i = 0; i < 10; i++)
    {
        if (strcmp(types[i], type) == 0)
            break;
        if (strcmp(types[i], "") == 0)
        {
            strcpy(types[i], type);
            break;
        }
    }
}
```
Function addType digunakan untuk menambahakan jenis genrenya seperti romance, action, school ,dan lainnya.

```c
void traverseFilesAndRemoveFile(char path[])
{
    DIR *dp;
    struct dirent *ep;

    dp = opendir(path);

    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            if (strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, ".") == 0 || !strstr(ep->d_name, ".png"))
                continue;

            char fullPath[100];
            strcpy(fullPath, path);
            strcat(fullPath, ep->d_name);

            remove(fullPath);
        }

        (void)closedir(dp);
    }
    else
        perror("Couldn't open the directory");
}
```

Function diatas meruakan function tranverFileAndRemoveFile yang digunakan untuk memilih file mana yang akan dapat melewati proses dan file mana yang seharunya dihapus. Karena di soal diminta hanya file .png saja yang dapat dilakukan memprosesan sedangkan yang lain tidak, maka file yang tidak memiliki .png akan diremove.

```c
void traverseAllTypes()
{
    for (int i = 0; i < 10; i++)
    {
        if (strcmp(types[i], "") == 0)
            break;
        char path[50] = "/home/rocuz99/shift2/drakor/";
        strcat(path, types[i]);
        strcat(path, "/data.txt");
        sortDataFile(path);
    }
}
```
Dalam function diatas digunakan untuk membaca jenis type dari suatu file untuk mendapatkan bagian akhir dari suatu string menggunakan strtcat.

```c
void sortDataFile(char path[])
{
    FILE *ptr;
    char str[100];
    ptr = fopen(path, "a+");

    PriorityQueue myPque;
    pqueue_init(&myPque);

    if (NULL == ptr)
    {
        printf("file can't be opened \n");
    }

    char title[50] = "";
    char category[50] = "";
    int year;

    while (fgets(str, 100, ptr) != NULL)
    {
        if (strcmp(str, "\n") != 0)
        {
            char *token = strtok(str, " ");
            if (strstr(str, "kategori"))
            {
                token = strtok(NULL, "\n");
                char buffer[50] = "";
                strcpy(buffer, token);
                strcpy(category, buffer);
                continue;
            }
            else if (strstr(str, "nama"))
            {
                token = strtok(NULL, " ");
                token = strtok(NULL, "\n");
                char buffer[50] = "";
                strcpy(buffer, token);
                strcpy(title, buffer);
                continue;
            }
            else
            {
                token = strtok(NULL, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, " ");
                char buffer[50];
                strcpy(buffer, token);
                year = atoi(buffer);
            }
            pqueue_push(&myPque, year, title);
        }
    }

    writeTitleToFile(path, category);

    while (!pqueue_isEmpty(&myPque))
    {
        int yearInt = pqueue_top_year(&myPque);
        char yearStr[10];

        sprintf(yearStr, "%d", yearInt);

        char title[100];
        strcpy(title, pqueue_top_title(&myPque));

        writeDataToFile(path, title, yearStr);

        pqueue_pop(&myPque);
    }

    fclose(ptr);
}
```
Pada function sortDataFile akan dilakukan sorting menggunakan priority queue dengan meemanggil fungsi pqueue_init(&myPque, dengan didapatkan nama file yang terdiri dari judul, genre, dan tahun menggunakan strtok(str, " ") untuk dapat memiahkan delimeternya dan setelah mendapatkan datanya, proses akan push data tersebut ke priority queue dan memanggil fungsi writeTitleToFile untuk dapat dipindahkan ke folder tujuan dengan sesuaiberdasarkan kategorinya. Namun, ketika terdapat data yang sama dalam priority queue makan akan dilakukan pop untuk memisahkannya.

```c
void traverseFilesAndDoSomething(char path[])
{
    DIR *dp;
    struct dirent *ep;

    dp = opendir(path);

    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            if (strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, ".") == 0 || !strstr(ep->d_name, ".png"))
                continue;

            char fullName[100] = "";
            strcpy(fullName, ep->d_name);

            char *end_token1;
            char *token1 = strtok_r(ep->d_name, "_", &end_token1);
            while (token1 != NULL)
            {
                // the-k2;2016;action.png
                char *end_token2;
                char *token2 = strtok_r(token1, ";", &end_token2);

                // title, year, genre
                char fileData[3][50] = {"", "", ""};

                int index = 0;
                while (token2 != NULL)
                {
                    strcpy(fileData[index], token2);
                    index++;
                    token2 = strtok_r(NULL, ";", &end_token2);
                }

                if (strstr(fileData[2], ".png"))
                    assignWithoutExtension(fileData[2]);

                addType(fileData[2]);

                // make directory
                char dirPath[50] = "/home/rocuz99/shift2/drakor/";
                strcat(dirPath, fileData[2]);
                makeDirectory(dirPath);

                // make data.txt
                strcat(dirPath, "/data.txt");
                if (access(dirPath, F_OK) == 0)
                {
                    writeDataToFile(dirPath, fileData[0], fileData[1]);
                }
                else
                {
                    makeFile(dirPath);
                    writeTitleToFile(dirPath, fileData[2]);
                    writeDataToFile(dirPath, fileData[0], fileData[1]);
                }

                // copy file
                char initialFilePath[100] = "/home/rocuz99/shift2/drakor/";
                strcat(initialFilePath, fullName);

                char targetFilePath[100] = "/home/rocuz99/shift2/drakor/";
                strcat(targetFilePath, fileData[0]);
                strcat(targetFilePath, ".png");
                copyFile(initialFilePath, targetFilePath);

                // move file
                strcpy(initialFilePath, targetFilePath); // swap init and target path

                strcpy(targetFilePath, "/home/rocuz99/shift2/drakor/");
                strcat(targetFilePath, fileData[2]);

                moveFile(initialFilePath, targetFilePath);

                token1 = strtok_r(NULL, "_", &end_token1);
            }
        }

        (void)closedir(dp);
    }
    else
        perror("Couldn't open the directory");
}
```
Function traverseFilesAndDoSomething digunakan untuk directory listing yang terjadi serta melalukan beberapa task yang lain. Dengan bantuan library `dirent.h`, file akan diiterasi dan namanya bisa diakses melalui `ep->d_name`.    
Di dalam fungsi ini dengan mendapatkan data dari suatu file dengan memisahkan tanda ; dari nama file tersebut. untuk mendapatkan data mengenai judul, tahun, dan genrenya. Setelah didapatkan datanya maka akan dilanjut dengan membuat directory dan mebuat file txt nya beserta isinya. Setelah itu file akan dicopy ke folder tujuan yang sesuai dengan genrenya.

## Soal 3
Pada soal ini, kami diminta mengklasifikasi file gambar hewan ke kategori darat dan air serta mencetak daftar ke dalam sebuah file khusus untuk hewan air.

Program kami dipecah menjadi fungsi-fungsi yang memiliki pekerjaan spesifik sehingga hanya tinggal dipanggil ketika dibutuhkan.

### Fungsi Main
```
makeDirectory("/home/rocuz99/modul2/darat");
sleep(3);
makeDirectory("/home/rocuz99/modul2/air");

unzip("/home/rocuz99/modul2", "./animal.zip");

traverseFilesAndDoSomething("/home/rocuz99/modul2/animal/", "categorizingAnimals");
traverseFilesAndDoSomething("/home/rocuz99/modul2/darat/", "removeBirdFile");

makeFile("/home/rocuz99/modul2/air/list.txt");

traverseFilesAndDoSomething("/home/rocuz99/modul2/air/", "writeAirAnimals");
```
Fungsi main menyimpan logika utama di program kami.  
Pertama, kami membuat dahulu folder `darat` dan `air` yang dijeda selama 3 detik menggunakan fungsi `sleep`. Lalu, kami unzip file `animal.zip`.  
Setelah semua file gambar animal sudah siap, kami melakukan directory listing untuk mengategorikan apakah termasuk darat, air, atau bukan keduanya. Kami lalu menghapus file hewan `bird` dengan strategi directory listing yang sama seperti sebelumnya.  
Setelah semua file sudah berada di kategori yang benar, kami membuat file `list.txt` di folder `air`. Kemudian, dengan strategi directory listing yang sama, kami menuliskan daftar file gambar di folder `air` ke dalam file `list.txt`.

### Fungsi makeDirectory
```
int status;
if (fork() == 0)
    execlp("mkdir", "mkdir", "-p", path, NULL);
while ((wait(&status)) > 0);
```
Fungsi ini berperan untuk membuat sebuah directory. Karena kami membutuhkan fungsi exec, maka kami memanfaatkan fork agar program tidak keluar sepenuhnya. Fungsi fork akan menghasilkan child process. Child process ini akan mengeksekusi mkdir dalam exec untuk membuat folder sesuai path yang di-passing. Parent process-nya sendiri akan menunggu child process selesai membuat folder, baru setelah itu keluar dari fungsi.

### Fungsi unzip
```
int status;
if (fork() == 0)
    execlp("unzip", "unzip", "-q", "-d", destination, target, NULL);
while ((wait(&status)) > 0);
```
Fungsi ini berperan untuk membuat meng-unzip file. Child process ini akan mengeksekusi unzip dalam exec untuk meng-unzip sesuai path file yang di-passing dan path tujuan sementara parent process menunggunya selesai.

### Fungsi moveFile
```
int status;
if (fork() == 0)
    execlp("mv", "mv", source, destination, NULL);
while ((wait(&status)) > 0);
```
Fungsi ini berperan untuk membuat memindahkan file (folder juga bisa). Child process ini akan mengeksekusi mv dalam exec untuk memindahkan file sesuai path source dan tujuan yang di-passing sementara parent process menunggunya selesai.

### Fungsi removeFile
```
int status;
if (fork() == 0)
    execlp("rm", "rm", "--force", source, NULL);
while ((wait(&status)) > 0);
```
Fungsi ini berperan untuk membuat menghapus file. Child process ini akan mengeksekusi rm dalam exec untuk menghapus file sesuai path di-passing sementara parent process menunggunya selesai.

### Fungsi makeFile
```
int status;
if (fork() == 0)
    execlp("touch", "touch", fileName, NULL);
while ((wait(&status)) > 0);
```
Fungsi ini berperan untuk membuat membuat file. Child process ini akan mengeksekusi touch dalam exec untuk membuat file sesuai path di-passing sementara parent process menunggunya selesai.

### Fungsi getUID
```
struct stat info;
int r;

r = stat(path, &info);
if (r == -1)
{
    fprintf(stderr, "File error\n");
    exit(1);
}

struct passwd *pw = getpwuid(info.st_uid);

return pw->pw_name;
```
Fungsi ini berperan untuk mengembalikan UID suatu file. Fungsi memanfaatkan bantuan dari library `sys/stat.h` dan `pwd.h`. Informsi UID didapat melalui `getpwudi`, tepatnya di dalam properti `pw_name`.

### Fungsi getFilePermission
```
struct stat fs;
int r;

r = stat(path, &fs);
if (r == -1)
{
    fprintf(stderr, "File error\n");
    exit(1);
}

if (fs.st_mode & S_IRUSR)
    strcat(permission, "r");
if (fs.st_mode & S_IWUSR)
    strcat(permission, "w");
if (fs.st_mode & S_IXUSR)
    strcat(permission, "x");

return permission;
```
Fungsi ini berperan untuk melihat permissions suatu file. Fungsi memanfaatkan bantuan dari library `sys/stat.h`. Di dalam pengkondisian, file akan dicek apakah memiliki permissions read, write, atau execute lalu di-concat ke dalam array of char permissions yang di-passing dan berupa string kosong awalnya. Akhirnya, semua permissions tersebut akan di-return.

### Fungsi formatFileName
```
char fullFileName[50] = "";
char permission[4] = "";
strcat(fullFileName, getUID(fullPath));
strcat(fullFileName, "_");
strcat(fullFileName, getFilePermission(fullPath, permission));
strcat(fullFileName, "_");
strcat(fullFileName, name);
fprintf(fptr, "%s", fullFileName);
fprintf(fptr, "%s", "\n");
```
Fungsi ini berperan untuk melihat mencetak daftar file sesuai format yang diminta soal, yaitu `UID_[UID file permission]_Nama File.[jpg/png]`. Diawali oleh string kosong, `fullFileName` akan akan di-append seterusnya.  
Pertama, UID file bisa didapat dari fungsi `getUID`, lalu file permission dari fungsi `getFilePermission`, serta nama filenya ari directory listing di fungsi `traverseFilesAndDoSomething`. Pada akhirnya, string tersebut di-write ke dalam file `list.txt`.

### Fungsi traverseFilesAndDoSomething
```
DIR *dp;
struct dirent *ep;

dp = opendir(path);

FILE *fptr; // only used in writeAirAnimals
if (strcmp(type, "writeAirAnimals") == 0)
{
    char pathListText[100] = "";
    strcat(pathListText, path);
    strcat(pathListText, "list.txt");
    fptr = fopen(pathListText, "w");
    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
}

if (dp != NULL)
{
    while ((ep = readdir(dp)))
    {
        if (strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, ".") == 0)
            continue;

        if (strcmp(type, "categorizingAnimals") == 0)
        {
            char source[100] = "";
            strcat(source, path);
            strcat(source, ep->d_name);
            char dest[100] = "/home/rocuz99/modul2/";
            if (strstr(ep->d_name, "darat"))
            {
                strcat(dest, "darat/");
                strcat(dest, ep->d_name);
                moveFile(source, dest);
            }
            else if (strstr(ep->d_name, "air"))
            {
                strcat(dest, "air/");
                strcat(dest, ep->d_name);
                moveFile(source, dest);
            }
            else
            {
                removeFile(source);
            }
        }
        else if (strcmp(type, "removeBirdFile") == 0)
        {
            char filePath[100] = "";
            strcat(filePath, path);
            if (strstr(ep->d_name, "bird"))
            {
                strcat(filePath, ep->d_name);
                removeFile(filePath);
            }
        }
        else if (strcmp(type, "writeAirAnimals") == 0)
        {
            if (strcmp(ep->d_name, "..") != 0 && strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "list.txt") != 0)
            {
                char fullPath[100] = "";
                strcat(fullPath, path);
                strcat(fullPath, "/");
                strcat(fullPath, ep->d_name);

                formatFileName(fullPath, ep->d_name, fptr);
            }
        }
        else
        {
            perror("Error: Type not found\n");
        }
    }

    (void)closedir(dp);
}
else
    perror("Couldn't open the directory");

if (strcmp(type, "writeAirAnimals") == 0)
{
    fclose(fptr);
}
```
Di fungsi ini, directory listing terjadi serta melalukan beberapa task yang lain. Dengan bantuan library `dirent.h`, file akan diiterasi dan namanya bisa diakses melalui `ep->d_name`.  
Terdapat tiga jenis tasks di sini, yaitu categorizingAnimals, removeBirdFile, dan writeAirAnimals.  
Fungsi categorizingAnimals akan memindahkan file sesuai foldernya (darat/air) atau dihapus. Nama file akan dicek menggunakan `strstr` apakah mengandung kata-kata darat atau air lalu dipindahkan. Jika tidak keduanya, maka akan dihapus.  
Fungsi removeBirdFile akan mencari nama file yang mengandung kata bird lalu menghapusnya.  
Fungsi writeAirAnimals akan mencetak semua file di folder animal ke dalam file `list.txt` dengan menggunakan fungsi formatFileName. Task ini tidak berlaku untuk direkctory `..` dan `.` serta file `list.txt` itu sendiri dengan dicek menggunakan `strcmp`.  
Buka tutup file `list.txt` sendiri tidak diletakkan di fungsi formatFileName karena akan terpanggil berkali-kali sedangkan buka tutup file merupakan fungsi yang sangat mahal sehingga cukup dipanggil sekali di awal fungsi.
