#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <wait.h>
#include <sys/types.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>

char types[10][20];

// Priority Queue code
typedef struct pqueueNode_t
{
    int year;
    char title[100];
    struct pqueueNode_t *next;
} PQueueNode;

typedef struct pqueue_t
{
    PQueueNode *_top;
    unsigned _size;
} PriorityQueue;

void pqueue_init(PriorityQueue *pqueue)
{
    pqueue->_top = NULL;
    pqueue->_size = 0;
}

bool pqueue_isEmpty(PriorityQueue *pqueue)
{
    return (pqueue->_top == NULL);
}

void pqueue_push(PriorityQueue *pqueue, int value, char title[])
{
    PQueueNode *temp = pqueue->_top;
    PQueueNode *newNode =
        (PQueueNode *)malloc(sizeof(PQueueNode));
    newNode->year = value;
    strcpy(newNode->title, title);
    newNode->next = NULL;

    if (pqueue_isEmpty(pqueue))
    {
        pqueue->_top = newNode;
        return;
    }

    if (value < pqueue->_top->year)
    {
        newNode->next = pqueue->_top;
        pqueue->_top = newNode;
    }
    else
    {
        while (temp->next != NULL &&
               temp->next->year < value)
            temp = temp->next;
        newNode->next = temp->next;
        temp->next = newNode;
    }
}

void pqueue_pop(PriorityQueue *pqueue)
{
    if (!pqueue_isEmpty(pqueue))
    {
        PQueueNode *temp = pqueue->_top;
        pqueue->_top = pqueue->_top->next;
        free(temp);
    }
}

int pqueue_top_year(PriorityQueue *pqueue)
{
    if (!pqueue_isEmpty(pqueue))
        return pqueue->_top->year;
    else
        return 0;
}

char *pqueue_top_title(PriorityQueue *pqueue)
{
    if (!pqueue_isEmpty(pqueue))
        return pqueue->_top->title;
    else
        return "";
}
// End of Priority Queue code

void makeDirectory(char path[])
{
    int status;
    if (fork() == 0)
        execlp("mkdir", "mkdir", "-p", path, NULL);
    while ((wait(&status)) > 0)
        ;
}

void makeFile(char fileName[])
{
    int status;
    if (fork() == 0)
    {
        execlp("touch", "touch", fileName, NULL);
    }
    while ((wait(&status)) > 0)
        ;
}

void copyFile(char source[], char destination[])
{
    int status;
    if (fork() == 0)
        execlp("cp", "cp", source, destination, NULL);
    while ((wait(&status)) > 0)
        ;
}

void moveFile(char source[], char destination[])
{
    int status;
    if (fork() == 0)
        execlp("mv", "mv", source, destination, NULL);
    while ((wait(&status)) > 0)
        ;
}

void unzip(char destination[], char target[])
{
    int status;
    if (fork() == 0)
        execlp("unzip", "unzip", "-q", "-d", destination, target, NULL);

    while ((wait(&status)) > 0)
        ;
}

void assignWithoutExtension(char genre[])
{
    char *token = strtok(genre, ".");
    strcpy(genre, token);
}

void writeDataToFile(char path[], char name[], char year[])
{
    FILE *fptr;
    fptr = fopen(path, "a");
    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }

    fprintf(fptr, "%s", "\n");
    fprintf(fptr, "%s", "nama : ");
    fprintf(fptr, "%s", name);
    fprintf(fptr, "%s", "\n");
    fprintf(fptr, "%s", "rilis : tahun ");
    fprintf(fptr, "%s", year);
    fprintf(fptr, "%s", "\n");

    fclose(fptr);
}

void writeTitleToFile(char path[], char genre[])
{
    FILE *fptr;
    fptr = fopen(path, "w");
    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }

    char text[100] = "kategori: ";
    strcat(text, genre);

    fprintf(fptr, "%s", text);
    fprintf(fptr, "%s", "\n");

    fclose(fptr);
}

void addType(char type[])
{
    // add type (romance, action, etc) to types array, but same value is not allowed
    for (int i = 0; i < 10; i++)
    {
        if (strcmp(types[i], type) == 0)
            break;
        if (strcmp(types[i], "") == 0)
        {
            strcpy(types[i], type);
            break;
        }
    }
}

void traverseFilesAndDoSomething(char path[])
{
    DIR *dp;
    struct dirent *ep;

    dp = opendir(path);

    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            if (strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, ".") == 0 || !strstr(ep->d_name, ".png"))
                continue;

            char fullName[100] = "";
            strcpy(fullName, ep->d_name);

            char *end_token1;
            char *token1 = strtok_r(ep->d_name, "_", &end_token1);
            while (token1 != NULL)
            {
                // the-k2;2016;action.png
                char *end_token2;
                char *token2 = strtok_r(token1, ";", &end_token2);

                // title, year, genre
                char fileData[3][50] = {"", "", ""};

                int index = 0;
                while (token2 != NULL)
                {
                    strcpy(fileData[index], token2);
                    index++;
                    token2 = strtok_r(NULL, ";", &end_token2);
                }

                if (strstr(fileData[2], ".png"))
                    assignWithoutExtension(fileData[2]);

                addType(fileData[2]);

                // make directory
                char dirPath[50] = "/home/rocuz99/shift2/drakor/";
                strcat(dirPath, fileData[2]);
                makeDirectory(dirPath);

                // make data.txt
                strcat(dirPath, "/data.txt");
                if (access(dirPath, F_OK) == 0)
                {
                    writeDataToFile(dirPath, fileData[0], fileData[1]);
                }
                else
                {
                    makeFile(dirPath);
                    writeTitleToFile(dirPath, fileData[2]);
                    writeDataToFile(dirPath, fileData[0], fileData[1]);
                }

                // copy file
                char initialFilePath[100] = "/home/rocuz99/shift2/drakor/";
                strcat(initialFilePath, fullName);

                char targetFilePath[100] = "/home/rocuz99/shift2/drakor/";
                strcat(targetFilePath, fileData[0]);
                strcat(targetFilePath, ".png");
                copyFile(initialFilePath, targetFilePath);

                // move file
                strcpy(initialFilePath, targetFilePath); // swap init and target path

                strcpy(targetFilePath, "/home/rocuz99/shift2/drakor/");
                strcat(targetFilePath, fileData[2]);

                moveFile(initialFilePath, targetFilePath);

                token1 = strtok_r(NULL, "_", &end_token1);
            }
        }

        (void)closedir(dp);
    }
    else
        perror("Couldn't open the directory");
}

void sortDataFile(char path[])
{
    FILE *ptr;
    char str[100];
    ptr = fopen(path, "a+");

    PriorityQueue myPque;
    pqueue_init(&myPque);

    if (NULL == ptr)
    {
        printf("file can't be opened \n");
    }

    char title[50] = "";
    char category[50] = "";
    int year;

    while (fgets(str, 100, ptr) != NULL)
    {
        if (strcmp(str, "\n") != 0)
        {
            char *token = strtok(str, " ");
            if (strstr(str, "kategori"))
            {
                token = strtok(NULL, "\n");
                char buffer[50] = "";
                strcpy(buffer, token);
                strcpy(category, buffer);
                continue;
            }
            else if (strstr(str, "nama"))
            {
                token = strtok(NULL, " ");
                token = strtok(NULL, "\n");
                char buffer[50] = "";
                strcpy(buffer, token);
                strcpy(title, buffer);
                continue;
            }
            else
            {
                token = strtok(NULL, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, " ");
                char buffer[50];
                strcpy(buffer, token);
                year = atoi(buffer);
            }
            pqueue_push(&myPque, year, title);
        }
    }

    writeTitleToFile(path, category);

    while (!pqueue_isEmpty(&myPque))
    {
        int yearInt = pqueue_top_year(&myPque);
        char yearStr[10];

        sprintf(yearStr, "%d", yearInt);

        char title[100];
        strcpy(title, pqueue_top_title(&myPque));

        writeDataToFile(path, title, yearStr);

        pqueue_pop(&myPque);
    }

    fclose(ptr);
}

void traverseAllTypes()
{
    for (int i = 0; i < 10; i++)
    {
        if (strcmp(types[i], "") == 0)
            break;
        char path[50] = "/home/rocuz99/shift2/drakor/";
        strcat(path, types[i]);
        strcat(path, "/data.txt");
        sortDataFile(path);
    }
}

void traverseFilesAndRemoveFile(char path[])
{
    DIR *dp;
    struct dirent *ep;

    dp = opendir(path);

    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            if (strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, ".") == 0 || !strstr(ep->d_name, ".png"))
                continue;

            char fullPath[100];
            strcpy(fullPath, path);
            strcat(fullPath, ep->d_name);

            remove(fullPath);
        }

        (void)closedir(dp);
    }
    else
        perror("Couldn't open the directory");
}

int main()
{
    makeDirectory("/home/rocuz99/shift2/drakor");
    unzip("/home/rocuz99/shift2/drakor", "./drakor.zip");

    traverseFilesAndDoSomething("/home/rocuz99/shift2/drakor/");

    traverseFilesAndRemoveFile("/home/rocuz99/shift2/drakor/");

    traverseAllTypes();

    return 0;
}